var GameController = (function() {

	var	game = document.querySelector(".game");

	var scoreboardListEl = document.getElementById("scoreboard-list");
	var scoreboardItemScript = document.getElementById("scoreboard-item-template");
	var scoreboardItemTemplate = Handlebars.compile(scoreboardItemScript.innerHTML);

	var areYouPro = false;

	// init
	function init(isPro) {

		areYouPro = isPro;

		// #hardtolivewithoutjquery
		document.onreadystatechange = () => {
			if (document.readyState === 'complete') {
				renderScoreboard();

				game.addEventListener("click", function() {
					playTheGame();
				});
			}
		};
	}

	// public, but not actually currently used
	function renderScoreboard() {	
		getScoresList().then(function(scores) {
            for (var i = 0, len = scores.length; i < len; i++) {
				renderScore(scores[i]);
			}
        });
	}

	function playTheGame() {
		var modifier = areYouPro ? 7000 : 20;
		var score = {
			playername: "you",
			score: Math.floor(Math.random() * modifier * 10) - 100,
			time: Math.floor(Math.random() * modifier)
		}
		saveScore(score);
	}

	// private
	function getScoresList() {
		return window.fetch('/scores')
			.then(function(response) {
                return response.json();
            });
	}

	function saveScore(score) {
		xhr = new XMLHttpRequest();
		var url = "/scores";
		xhr.open("POST", url, true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.onreadystatechange = function () { 
		    if (xhr.readyState == 4) {
		    	if (xhr.status == 200) {
			        renderScore(score);
			    } else if (xhr.status == 500) {
	            	alert('Something bad happened.');
	            } else {
	            	alert('Something magical happened.');
	            }
	        }
		}
		var data = JSON.stringify(score);
		xhr.send(data);
	}

	function renderScore(score) {
		var wrapper = document.createElement('div');
		var	scoreHtml = scoreboardItemTemplate(score);
		wrapper.innerHTML = scoreHtml;
		scoreboardListEl.append(wrapper.children[0]);
	}

	return {
		init: init
	}

})();
