var express = require('express');
var router = express.Router();

router.route('/').get(function(req, res) {
  var data = {};
  var gameName = "GOTY";
  data.title = gameName + ": The Game";
  data.gameName = gameName;
 
  res.render('game', data);
});

router.route('/:gameName').get(function(req, res) {
  var data = {};

  var gameName = req.params.gameName ? req.params.gameName : "GOTY";
  data.title = gameName + ": The Game";
  data.gameName = gameName;
  data.isPro = req.query.pro != null;
  res.render('game', data);
});


module.exports = router;
